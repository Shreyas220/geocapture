import os

import utils.prometheus as prometheus

# utility to parse_ip
def parse_ip(addr: str):
    formatted_ip = b""
    for ip_class in range (0,4):
        formatted_ip = formatted_ip + str(addr & 0xFF).encode()
        if ip_class != 3:
            formatted_ip = formatted_ip + b"."
        addr = addr >> 8

    return formatted_ip

# utility to get data packet size
output_power_from_bytes = {"b": 0, "kb": 1, "mb": 2, "gb": 3}
def datatype_conversion(size_in_bytes: int, output_data_unit: str):
    return size_in_bytes / (1024 ** output_power_from_bytes[output_data_unit])

# Skip querying the IP if we already know the IP's location
def find_or_send_location(ip_address: str, location_of_ip: dict, unable_to_find: set, ip_yet_to_find: set, worldmap):

    if not location_of_ip.get(ip_address) and ip_address not in unable_to_find:
        ip_yet_to_find.add(ip_address)

    elif ip_address not in unable_to_find:
        (city, latitude, longitude) = location_of_ip.get(ip_address)

        prometheus.send_data_to_geomap(
            worldmap, city, latitude, longitude)
