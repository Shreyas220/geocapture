import threading
import ipinfo

import utils.prometheus as prometheus
import utils.network as network
import config.config as cfg


# Define an isolated thread function to run every 3 seconds that parallely queries the IP addresses for their location and pushed to Prometheus
def find_and_send_new_location(ip_yet_to_find: set, unable_to_find: set, worldmap, location_of_ip: dict):
    ipinfo_client = ipinfo.getHandler(access_token=cfg.ip_info_api_key)

    threading.Timer(
            3.0,
            find_and_send_new_location,
            args=[ip_yet_to_find,
                  unable_to_find,
                  worldmap,
                  location_of_ip]
            ).start()

    # a daemon that will exit once we call sys exit
    threading.Timer.daemon = True

    for ip in ip_yet_to_find.copy():
        # get info form the ipinfo API
        details = ipinfo_client.getDetails(ip).all

        # request on private network
        try:
            if details["bogon"]:
                pass
        except KeyError:
            pass

        try:
            if details == None:
                unable_to_find.add(ip)
            else:
                prometheus.send_data_to_geomap(
                        worldmap,
                        details["city"],
                        details["latitude"],
                        details["longitude"]
                        )

                location_of_ip[ip] = [
                        details["city"],
                        details["latitude"],
                        details["longitude"]
                        ]

            ip_yet_to_find.remove(ip)

        except KeyError:
            # Happens when concurrently the thread already removed it since this is happening on the original data
            pass
