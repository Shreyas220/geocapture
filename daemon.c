#include <linux/bpf.h>
#include <linux/icmp.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>

struct packet {
  unsigned int ip;
  unsigned int dest_ip;
  unsigned int bytes;
  unsigned int protocol;
  unsigned int dest_port;
  unsigned int status;
};

struct FilterRule {
  unsigned int dest_port;
  unsigned int protocol;
  u32 source_ip;
};

// ring buffer stores the packet data
BPF_RINGBUF_OUTPUT(events, 128);

#define RULE_LENGTH 10

BPF_ARRAY(filter_rules, struct FilterRule, RULE_LENGTH);

static void drop_log(unsigned int ip, unsigned int proto, unsigned int port) {
  bpf_trace_printk("Dropped packet - IP:%u, Protocol:%u, Port:%u", ip, proto,
                   port);
}

static void push_log(struct packet *pk) {
  struct packet *ip_and_byte = events.ringbuf_reserve(sizeof(struct packet));
  if (!ip_and_byte) {
    return;
  }

  ip_and_byte->ip = pk->ip;
  ip_and_byte->bytes = pk->bytes;
  ip_and_byte->dest_ip = pk->dest_ip;
  ip_and_byte->protocol = pk->protocol;
  ip_and_byte->dest_port = pk->dest_port;
  ip_and_byte->status = pk->status;

  events.ringbuf_submit(ip_and_byte, 0);
}

int intercept_packets(struct xdp_md *ctx) {
  // We mark the start and end of our ethernet frame
  void *ethernet_start = (void *)(long)ctx->data;
  void *ethernet_end = (void *)(long)ctx->data_end;

  struct ethhdr *ethernet_frame = ethernet_start;

  // Check if we have the entire ethernet frame
  if ((void *)ethernet_frame + sizeof(*ethernet_frame) <= ethernet_end) {
    struct iphdr *ip_packet = ethernet_start + sizeof(*ethernet_frame);

    // Check if the IP packet is within the bounds of ethernet frame
    if ((void *)ip_packet + sizeof(*ip_packet) <= ethernet_end) {

      // extract info from the IP packet
      struct packet pk;
      pk.ip = ip_packet->saddr;
      pk.bytes = (ethernet_end - ethernet_start);
      pk.dest_ip = ip_packet->daddr;
      pk.protocol = ip_packet->protocol;
      pk.dest_port = 0;

      // bpf_trace_printk("%d", dest_ip);
      // bpf_trace_printk("%d", protocol);

      // check the protocol and get port
      if (pk.protocol == IPPROTO_TCP) {
        struct tcphdr *tcp = (void *)ip_packet + sizeof(*ip_packet);
        if ((void *)tcp + sizeof(*tcp) <= ethernet_end) {
          // Checking if the destination port matches with the specified port
          pk.dest_port = htons(tcp->dest);
        }
      }

      if (pk.protocol == IPPROTO_UDP) {
        struct udphdr *udp = (void *)ip_packet + sizeof(*ip_packet);
        if ((void *)udp + sizeof(*udp) <= ethernet_end) {
          // Checking if the destination port matches with the specified port
          pk.dest_port = htons(udp->dest);
        }
      }

      // check if we need to drop the packet
      // TODO: use bpf_loop or bpf_for_each_map_elem
      // bpf_loop(32, &check, NULL, 0);
      struct FilterRule *fr;
      int i;
#pragma unroll
      for (i = 0; i < RULE_LENGTH; i++) {
        // bpf_trace_printk("hello");
        int j = i;
        fr = filter_rules.lookup(&j);
        if (fr) {
          // bpf_trace_printk("log: %u, %u", fr.dest_port, dest_port);
          //  match source ip
          unsigned int src_ip = htonl(pk.ip);
          if (fr->source_ip == src_ip) {
            if (fr->protocol == pk.protocol) {
              if ((fr->dest_port == pk.dest_port && pk.dest_port != 0) ||
                  fr->dest_port == 0) {
                pk.status = 0;
                push_log(&pk);
                drop_log(src_ip, pk.protocol, pk.dest_port);
                return XDP_DROP;
              } else {
                break;
              }
            } else if (fr->protocol == 0 && (pk.protocol == IPPROTO_TCP ||
                                             pk.protocol == IPPROTO_UDP)) {
              if ((fr->dest_port == pk.dest_port && pk.dest_port != 0) ||
                  fr->dest_port == 0) {
                pk.status = 0;
                push_log(&pk);
                drop_log(src_ip, pk.protocol, pk.dest_port);
                return XDP_DROP;
              } else {
                break;
              }
            } else {
              break;
            }
          } else {
            // source IP did not match
            if (fr->protocol == pk.protocol) {
              if ((fr->dest_port == pk.dest_port && pk.dest_port != 0) ||
                  fr->dest_port == 0) {
                pk.status = 0;
                push_log(&pk);
                drop_log(src_ip, pk.protocol, pk.dest_port);
                return XDP_DROP;
              } else {
                break;
              }
            } else if (fr->protocol == 0 && (pk.protocol == IPPROTO_TCP ||
                                             pk.protocol == IPPROTO_UDP)) {
              if ((fr->dest_port == pk.dest_port && pk.dest_port != 0) ||
                  fr->dest_port == 0) {
                pk.status = 0;
                push_log(&pk);
                drop_log(src_ip, pk.protocol, pk.dest_port);
                return XDP_DROP;
              } else {
                break;
              }
            } else {
              break;
            }
          }
        }
      }
      pk.status = 1;
      push_log(&pk);
    }
  }

  return XDP_PASS;
}
