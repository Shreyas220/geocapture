import os, sys, ipaddress

import yaml
#from dotenv import load_dotenv

network_device = "wlan0"
output_data_unit = "kb"
ip_info_api_key = ""

# to store multiple filter rule maps
filter_list = []

def error(err, exit_code):
    print(err)
    if exit_code != 0:
        print("EXITING...")
        sys.exit(exit_code)

def init_config():
    global network_device, output_data_unit, ip_info_api_key
    # try to read file
    try:
        f = open("geocapture.yaml", "r")
    except IOError:
        error("Error while trying to read configuration file...", 1)

    # try to get config and return error
    try:
        config = yaml.load(f, yaml.SafeLoader)
    except yaml.YAMLError as exc:
        print ("Error while parsing YAML file:")
        if hasattr(exc, 'problem_mark'):
            if exc.context != None:
                print ('  parser says\n' + str(exc.problem_mark) + '\n  ' +
                    str(exc.problem) + ' ' + str(exc.context) +
                    '\nPlease correct data and retry.')
            else:
                print ('  parser says\n' + str(exc.problem_mark) + '\n  ' +
                    str(exc.problem) + '\nPlease correct data and retry.')
        else:
            print ("Something went wrong while parsing yaml file")
        sys.exit(1)

    # read generic config values
    try:
        generic_config = config["generic"]
    except KeyError or None:
        error("Error while parsing generic configuration...", 1)

    try:
        network_device = generic_config["network_device"]
        print("Monitoring network interface: ", network_device)
    except KeyError or None:
        print("No network interface specified, using: ", network_device)

    try:
        output_data_unit = generic_config["output_data_unit"]
        print("Setting output data unit to: ", output_data_unit)
    except KeyError or None:
        print("No data unit specified, using: ", output_data_unit)

    try:
        ip_info_api_key = generic_config["ipinfo_api_key"]
        print("API key for ipinfo.io found")
    except KeyError or None:
        error(
        """IPINFO_API_KEY unset. Geocapture needs ipinfo API key to query location of IP addresses.\n
        Generate by registering on https://ipinfo.io""", 1)

    # read filter config
    try:
        filter_config = config["filters"]
    except KeyError or None:
        error("Error while parsing filter config...", 1)

    print(filter_config)
    for rule in filter_config:
        fil = {}
        try:
            fil["port"] = int(rule["port"])
            if fil["port"] <= 1024 or fil["port"] > 65535:
                error("Invalid port number...", 1)
        except KeyError or None:
            fil["port"] = "*"

        try:
            fil["protocol"] = rule["protocol"].lower()
            if fil["protocol"] != "tcp" and fil["protocol"] != "udp":
                error("Invalid protcol. Must be tcp or udp", 1)
        except KeyError or None:
            fil["protocol"] = "*"


        try:
            fil["source_ip"] = rule["source_ip"]
            try:
                ipaddress.ip_address(fil["source_ip"])
            except ValueError:
                error("Invalid IP address")
        except KeyError or None:
            fil["source_ip"] = "*"

        print("config:", fil["source_ip"])

        filter_list.append(fil)

    print("FILTER LIST: ", filter_list)
