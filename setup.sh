sudo pacman --noconfirm -Sy reflector --needed
sudo reflector --verbose --sort age -p "https" -l 100 --score 15 --download-timeout 30 --save /etc/pacman.d/mirrorlist

# dev deps
#sudo pacman -S --noconfirm bcc bcc-tools python-bcc python-pip docker docker-compose --needed

#sudo pip3 install -r /vagrant/requirements.txt

sudo pacman -S --noconfirm docker docker-compose tmux --needed

sudo systemctl enable --now docker.service
