import os, signal, sys, time, socket, ipaddress, ctypes
from bcc import BPF

import utils.network as network
import utils.protocol_utils as protocol_utils
import utils.prometheus as prometheus
import utils.thread as thread
import config.config as cfg

# parsing configuration from environment
cfg.init_config()

# loading eBPF code
bpf_source = BPF(src_file="daemon.c", cflags=["-O2"])
fn_intercept = bpf_source.load_func("intercept_packets", BPF.XDP)

# attaching to xdp
print("ATTACHING BPF FUNCTION to XDP HOOK...")
bpf_source.attach_xdp(cfg.network_device, fn_intercept, 0)
print("SUCCESSULLY ATTACHED HOOK...")

filter_rules = bpf_source.get_table("filter_rules")
rule_idx = 0

class FilterRule(ctypes.Structure):
    _fields_ = [
        ('dest_port', ctypes.c_uint),
        ('protocol', ctypes.c_uint),
        ('source_ip', ctypes.c_uint32),
    ]

if len(cfg.filter_list) > 0:
    for fil in cfg.filter_list:
        print("SOURCE IP:", fil["source_ip"])
        if fil["source_ip"] != "*":
            #ip = int.from_bytes(socket.inet_aton(fil["source_ip"]), "big")
            ip = int(ipaddress.ip_address(fil["source_ip"]))
            print(ip)
        else:
            ip = 0

        if fil["port"] != "*":
            port = int(fil["port"])
        else:
            port = 0

        if fil["protocol"] == "tcp":
            protocol = socket.IPPROTO_TCP
        elif fil["protocol"] == "udp":
            protocol = socket.IPPROTO_UDP
        else:
            protocol = 0

        filter_rules[int(rule_idx)] = FilterRule(port, protocol, ip)
        rule_idx += 1

packet_count = 0
start_time = time.time()

ip_location = {}
ip_yet_to_find = set()
unable_to_find = set()

# parsing the packet data
def parse_frame(_, data, size):
    packet = bpf_source["events"].event(data)

    ip = network.parse_ip(packet.ip).decode()
    protocol = protocol_utils.translate_protocol(packet.protocol)
    data_transmitted = network.datatype_conversion(packet.bytes, cfg.output_data_unit)

    dest_ip = network.parse_ip(packet.dest_ip).decode()
    if packet.status == 0:
        status = "DROPPED"
    else:
        status = "PASSED"

    global packet_count, start
    packet_count += 1
    print("|{:<5}|{:<12f}|{:<16}|{:<16}|{:<5}|{:<10}|{:<12} {}|{:<7}|".format(
        packet_count,
        time.time() - start_time,
        ip,
        dest_ip,
        packet.dest_port,
        protocol,
        data_transmitted,
        cfg.output_data_unit,
        status))

    prometheus.send_data_for_total_bytes(data_per_ip, ip, data_transmitted)
    network.find_or_send_location(ip, ip_location, unable_to_find, ip_yet_to_find, worldmap)
    prometheus.send_data_for_protocols(protocols_used, protocol)

def cleanup():
    print("\nCLEANING UP...")
    print("STOPPING MONITOR...")
    print("UNHOOKING NETWORK DEVICE...")
    bpf_source.remove_xdp(cfg.network_device, 0)
    print("EXITING...")
    sys.exit(0)

print("STARTING GEOCAPTURE eBPF DAEMON...")
print("|{:<5}|{:<12}|{:<16}|{:<16}|{:<5}|{:<10}|{:<15}|{:<7}|".format('NO.', 'TIME', 'SOURCE ADDRESS', 'DEST. ADDRESS', 'PORT', 'PROTOCOL', 'SIZE', 'STATUS'))
bpf_source["events"].open_ring_buffer(parse_frame)

signal.signal(signal.SIGINT, cleanup)
signal.signal(signal.SIGTERM, cleanup)

data_per_ip, worldmap, protocols_used = prometheus.init_prom_client(8000)

# Launch the thread
thread.find_and_send_new_location(
    ip_yet_to_find,
    unable_to_find,
    worldmap,
    ip_location)

while 1:
    # start polling the ring buffer for events
    bpf_source.ring_buffer_poll()

#try:
#    while 1:
#        bpf_source.ring_buffer_poll()
#except KeyboardInterrupt:
